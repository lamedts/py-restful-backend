import falcon
import json
import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.FileHandler('response.log'))
logger.setLevel(logging.INFO)

class ResponseLoggerMiddleware(object):
    def process_response(self, req, resp):
        logger.info('{0} {1} {2}'.format(req.method, req.relative_uri, resp.status[:3]))

class HomeResource:
    def on_post(self, req, resp):
        resp.body = json.dumps({"doc": True}, ensure_ascii=False)

class Resource:
    def on_get(self, req, resp):
        resp.body = json.dumps({"doc": True}, ensure_ascii=False)

def handle_404(req, resp):
    print(req)
    print(resp)
    resp.status = falcon.HTTP_404
    resp.body = json.dumps({"code": 404})
    
def capture_error(ex, req, resp, params):
    print(ex)
    print(req)
    print(resp)
    print(params)
    if isinstance(ex, falcon.HTTPError):
        print(type(ex))
        print(falcon.HTTPError)
    resp.status = falcon.HTTP_723
    resp.body = 'error: %s' % str(ex)

class cap_error(Exception):
    @staticmethod
    def handle(ex, req, resp, params):
        print(Exception)
        print(ex)
        print(req)
        print(resp)
        print(params)
        resp.status = falcon.HTTP_723
        resp.body = 'error: %s' % str(ex)


app = falcon.API(middleware=[ResponseLoggerMiddleware()])
app.add_route('/', HomeResource())
app.add_route('/test', Resource())
app.add_error_handler(Exception, capture_error)
# app.add_error_handler(cap_error)

# any other route should be placed before the handle_404 one
app.add_sink(handle_404, '')

