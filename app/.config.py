import os

class BaseConfig(object):

   PROJECT = "app" 
   PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
   DEBUG = False
   TESTING = False
   ADMINS = ['hello@app.io']
   SECRET_KEY = 'secret key'
   

class DefaultConfig(BaseConfig):

   WTF_CSRF_ENABLED = True
   DEBUG = True
   WTF_CSRF_ENABLED = True
   SECRET_KEY = 'development key'
   DB_CONFIG = {}
