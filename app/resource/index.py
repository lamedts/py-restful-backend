from flask_jwt_extended import jwt_required, create_access_token, get_jwt_identity
from flask_restful import Resource

class PrivateResource(Resource):
    @jwt_required
    def get(self):
        print(get_jwt_identity())
        return {"meaning_of_life": 42}

class Index(Resource):
    def get(self):
        return {"response": "data"}, 200
        