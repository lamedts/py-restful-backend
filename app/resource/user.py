from flask_jwt_extended import jwt_required, create_access_token, get_jwt_identity, jwt_optional
from flask_restful import Resource
from flask import abort

class User_Obj(object):
    def __init__(self, id, username, password):
        self.id = id
        self.username = username
        self.password = password

    def __str__(self):
        return "User(id=\"%s\")" % self.id

users = [
    User_Obj(1, "user1", "abcxyz"),
    User_Obj(2, "user2", "abcxyz"),
]

username_table = {u.username: u for u in users}
userid_table = {u.id: u for u in users}

def authenticate(username, password):
    user = username_table.get(username, None)
    # if user and safe_str_cmp(user.password.encode("utf-8"), password.encode("utf-8")):
    if user and user.password.encode("utf-8") == password.encode("utf-8"):
        return user

def identity(payload):
    user_id = payload["identity"]
    return userid_table.get(user_id, None)

class User(Resource):
    @jwt_optional
    def get(self, action=None):
        current_user = get_jwt_identity()
        if current_user and action != None and action != 'signin' and action != 'signup' :
            return jsonify({'hello_from': current_user}), 200
        return {"meaning_of_life": 42}

    def post(self, action=None):
        print(get_jwt_identity())
        if  action != None:
            if action == 'signin':
                return {"status": "signin"}
            elif action == 'signup' :
                return {"status": "signup"}
        abort(404)

    @jwt_required
    def put(self):
        print(get_jwt_identity())
        return {"meaning_of_life": 42}
    
    @jwt_required
    def delete(self):
        print(get_jwt_identity())
        return {"meaning_of_life": 42}
