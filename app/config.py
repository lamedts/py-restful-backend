import os

class Config(object):
    PROJECT = "app" 
    PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
    DEBUG = False
    TESTING = False
    ADMINS = ['hello@app.io']
    SECRET_KEY = 'default_secret_key'

class ProductionConfig(Config):
    SECRET_KEY = os.getenv("SECRET_KEY")

class DevelopmentConfig(Config):
    SECRET_KEY = os.getenv("SECRET_KEY")
    DEBUG = True

class TestingConfig(Config):
    SECRET_KEY = os.getenv("SECRET_KEY")
    TESTING = True