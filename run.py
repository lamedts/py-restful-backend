import os
from app import create_app
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

app = create_app()

if __name__ == '__main__':

    app.run(
        host  = os.getenv("HOST"), 
        port  = int(os.getenv("PORT"))
    )
   