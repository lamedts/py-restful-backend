# Restful Backend

# To run
```bash
# set up your own dev workspace
# install the modules
$ python run.py
```

# Miscs
Authorization:JWT {{JWT}}
python src/srv/py

3 space indent (no tab)
prefer longer names for the sake of clarity and readability (UserModels over models)
single quotation 'string' for string
Naming convention: module_name, ClassName, method_name, function_name, CONSTANT_NAME, local_variable_name